local function runCommand(Command)
   local outerr = os.tmpname()
   local code = os.execute(Command .. " > " .. outerr .. " 2>&1")
   local file = io.open(outerr)
   local content = file:read("*a")
   file:close()
   os.remove(outerr)
   return code, content
end

function GITcheckoutMainBranch(BuildLocation, MainBranchName)
   local Script = "cd " .. BuildLocation
   if DIR_SEP == "\\" then
      Script = Script ..  " && git reset --hard > NUL && git clean -dfx > NUL && git checkout " .. MainBranchName
   else
      Script = Script ..  " && git reset --hard && git clean -dfx > /dev/null && git checkout " .. MainBranchName
   end
   local Code, Err = runCommand(Script)
   if Code ~= 0 then
      iguana.logError("Could not checkout " .. MainBranchName .. ": " ..Err)
      error("Could not checkout " .. MainBranchName)
      return
   end
end

function GITpull(BuildLocation)
   local Script = "cd " .. BuildLocation .. " && git pull"
   local Code, Err = runCommand(Script)
   if Code ~= 0 then
      iguana.logError("Could not pull: " ..Err)
      error("Could not pull")
      return
   end
end

function GITcheckoutCommit(BuildLocation, Commit)
   local Script = "cd " .. BuildLocation .. " && git checkout " .. Commit
   local Code, Err = runCommand(Script)
   if Code ~= 0 then
      iguana.logError("Invalid commit id: " .. Commit .. ": " ..Err)
      --error("Invalid commit id: " .. Data)
      return
   end
end

function GITcommitReadLog(BuildLocation, Commit)
   local Script = 'cd ' .. BuildLocation .. ' && git --no-pager log --format="%B%cn%n%ce" -n 1 ' .. Commit
   f = io.popen(Script)
   local LogMsg = f:read("*a")
   f:close()
   return LogMsg
end