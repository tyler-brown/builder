local function runCommand(Command)
   iguana.setTimeout(60*1000)
   local outerr = os.tmpname() 
   local code = os.execute(Command .. " > " .. outerr .. " 2>&1")
   local file = io.open(outerr)
   local content = file:read("*a")
   file:close()
   os.remove(outerr)
   return code, content
end

function buildSpecifiedCommit(BuildLocation, ShortCommitId, BuildResultsLocation, FullCommitId)
   local ServiceEnv = ""
   local platform = os.whichOs()
   if "Windows" == platform then
      -- VS build environment is not available to Iguana Windows service, need to load it
      ServiceEnv = ' && "C:\\Program Files (x86)\\Microsoft Visual Studio\\2022\\BuildTools\\VC\\Auxiliary\\Build\\vcvars64.bat"'
   elseif "macOS" == platform then
      -- dmgbuild is not visiable to Iguana Launchd service, need to modify path
      ServiceEnv = ' && PATH=$PATH:/usr/local/bin/'
   end
   local Script = "cd " .. BuildLocation .. ServiceEnv .. ' && sh posix_build.sh ' .. BuildResultsLocation .. ShortCommitId
   local Code, Err = runCommand(Script)
   iguana.log(Err)
   if Code ~= 0 then
      iguana.logError("Build failed: '" .. Err .. "' CommitId: " .. FullCommitId)
      error("Build failed!")
      return
   end
end

function pushSpecifiedCommit(RepoPath, ShortCommitId, BuildResults, FullCommitId, PushKey, PushDest)
   trace(RepoPath, ShortCommitId, BuildResults, FullCommitId, PushKey, PushDest)
   local installer
   local platform = os.whichOs()
   if platform == "macOS" then
      installer = RepoPath .. "dmg_installer/iguana_macos_x64_setup.dmg"
   elseif platform == "Linux" then
      installer = RepoPath .. "Iguana/iguana_linux_x64.tar.gz"
   elseif platform == "Windows" then
      installer = RepoPath .. "iguana_installer_nsis\\iguana_windows_x64_setup.exe"
   end
   trace(installer)
   local dest = BuildResults .. ShortCommitId

   -- copy installer to build result folder
   local code, err = runCommand("cp -P " .. installer .. " " .. dest)
   if code ~= 0 then
      iguana.logError("Failed to copy installer: " .. err .. " CommitId: " .. FullCommitId)
      --error("Failed to copy installer!")
      --return
   end

   -- scp build results to server
   code, err = runCommand("scp -r -o StrictHostKeyChecking=no -i "..PushKey.." "..dest.." "..PushDest..platform)
   if code ~= 0 then
      iguana.logError("Failed to push build results: '" .. err .. "' CommitId: " .. FullCommitId)
      --error("Failed to push build results!")
      return
   end
end

function cleanUpOldResults(BuildResultsLocation, LifeSpan)
   if not os.fs.stat(BuildResultsLocation) then
      return
   end
   for FileName, FileInfo in os.fs.glob(BuildResultsLocation .. "*") do
      trace(FileName)
      trace(FileInfo)
      if os.time() - FileInfo["ctime"] > LifeSpan then -- this is older than a week
         os.execute("rm -rf " .. FileName)
      end
   end
end

function countBuildError(BuildResultsLocation, ShortCommitId)
   if not os.fs.stat(BuildResultsLocation) then
      return 0
   end
   local Count = 0
   for FileName, FileInfo in os.fs.glob(BuildResultsLocation .. ShortCommitId .. DIR_SEP .. "*") do
      Count = Count + 1
   end
   return Count
end